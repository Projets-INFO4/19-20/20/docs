# Observations API Software Heritage



## Fonctionnement de la recherche sur le site web

Exemple pour une recherche avec le mot clé "doom"

1. Requête api /resolve/doom. "doom" est utilisé comme *swh_id*. 

   * Réponse : 400 Bad Request

2. Requête api /origin/search/doom. "doom" est utilisé comme *url_pattern*

   * Réponse : 200. Renvoie une liste de repos github contenant le pattern dans son  URL

3. Pour chaque URL github renvoyé par la requête search, on effectue une requête api pour en récupérer un snapshot. Requête api : /origin/{url}/visit/latest?require_snapshot

4. Après avoir récupéré un snapshot on effectue une requête vers l'API Vault qui va générer la tarball

   * Requête GET: Donne le statut de la génération de la tarball / Donne le lien de téléchargement une fois la génération terminée
     * Réponse 200: Le paquet est prêt et est déjà cooked
     * Réponse 404: Le paquet n'est pas encore cooked
     * Réponse 400: L'id n'est pas reconnue
   * Requête POST: Demande le cooking d'une tarball