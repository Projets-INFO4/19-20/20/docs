# Changelog

## Semaine 1 (27 janv.)

* Nous avons eu une réunion avec Olivier Richard. Nous avons parlé des objectifs de notre projet.
  
  * Tout d'abord, nous devons nous familiariser avec nix et nix-pkgs. Il faut faire attention car la prise en main de nix peut être fastidieuse.
  
  * Nous devons également analyser Software Heritage (SwH) afin de comprendre son fonctionnement.
  
  * Une fois que nous serons un peu plus familiers avec les outils que nous utiliserons, nous devrons observer la mise en œuvre de guix de SwH car nix et guix sont assez similaires.
  
  * A un moment donné, nous devrons probablement aider le projet 10 à utiliser nix.
  
  * Enfin, l'objectif principal de ce projet est de créer un support pour SwH dans nix et de l'ajouter à nix-pkgs.

## Semaine 2 (3 fev.)

* Nous nous sommes penchés sur le fonctionnement des paquets nix et avons essayé de modifier le paquet "hello"
  * En essayant de modifier le fichier default.nix nous avons vu qu'il y avait deux paramètres : l'url des sources et un sha. En modifiant l'url uniquement nous n'avons pas l'air de pouvoir changer le contenu du paquet. Est-ce qu'il faut donc modifier de la combinaison des deux ?
  * En clonant le repo nix-pkgs pour développer localement, nous n'arrivons pas à utiliser cette version locale.

## Semaine 3 (10 fev.)

* Nous avons essayé de créer/modifier un paquet nix à partir d'un de nos repo git.
  * Tentative accessible ici : [Draft](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/19-20/20/draft).
    * Bug actuel avec le builder, il n'arrive pas à décompresser après avoir fetch. 

## Semaine 4 (17 fev.)

* Étude du paquet guix s'appuyant sur Software Heritage.

* Étude et tests poussés de l'API pour récupérer des tarball à partir de mots-clés.

## Semaine 6 (3 mar.)

* Correction d'un bug dans le paquet d'exemple utilisant fetchgit.

* Continuation des tests API

* Ajout d'un squelette de paquet fetch-swh dans nixpkgs.
  * Tentative accessible ici : [nixpkgs_swh](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/19-20/20/nixpkgs_swh).

* Préparation de la soutenance du 9 mars.

## Semaine 7 (9 mar.)

* Soutenance de mi-parcours.
* Avancement de la première version du paquet fetchswh

## Semaine 8 (12 mar.)
* Début du confinement, pas d'avancement

## Semaine 9 (23 mar.)
* Ecriture d'un test pour tester notre première version de fetchswh
* Avancement de la première version du paquet fetchswh
  * ~~problème sur comment structurer le descripteur de paquet~~
  * Paquet et exemple importés sur la branche fetchswh\_test de [nixpkgs_swh](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/19-20/20/nixpkgs_swh)
   * ~~Souci avec la gestion du hash au moment du téléchargement~~

## Semaine 10 (30 mar.)
* Tentatives de correction du souci de hash

## Semaine 11 (6 avr.)
* Correction du souci de hash. En utilisant fetchTarball, on arrive à passer outre. V1 du fetcher fonctionnel.

## Semaine 12 (13 avr.)
* Commencement de la V2 du fetcher (Gestion du cooking de la tarball). Disponible sur la branche fetchswh-v2 de [nixpkgs_swh](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/19-20/20/nixpkgs_swh/-/tree/fetchswh-v2)
* Souci avec le script du builder, impossibilité d'utiliser curl

## Semaine 13 (20 avr.)
* Tentatives de correction du souci curl, mais echec (curl: (6) could not resolve host)
  * Possibilité que ce soit un souci de passthrough ?
* Commencement du rapport

## Semaine 14 (27 avr.)
* Finition du rapport. Fin du projet
* Manuel pour la demo disponible dans le répertoire Demo
