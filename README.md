# Polytech Grenoble, INFO4 Project 2019-2020

## Fetch-Swh

Fetch-swh main goal is to provide a fetcher for [Software Heritage](https://www.softwareheritage.org) in [Nixpkgs](https://github.com/NixOS/nixpkgs).
As such, it makes possible for someone to get a tarball for a specific release of softwares hosted on Software Heritage in Nix packages.

Software Heritage is an initiative whose goal is to collect, preserve, and share software code—both freely licensed and not—in a universal software storage archive.
Nix is a cross-platform package manager that utilizes a purely functional deployment model where software is installed into unique directories generated through cryptographic hashes.
