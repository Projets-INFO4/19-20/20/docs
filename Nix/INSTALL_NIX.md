# Installation



## Install Nix

```bash
curl https://nixos.org/nix/install | sh
echo ". $HOME/.nix-profile/etc/profile.d/nix.sh" >> ~/.bashrc
source ~/.bashrc 
```

## Install a package

### Look for a package

```bash
nix search <keyword>
```

### Intall a package

```bash
nix-env -iA <package>
```

### Uninstall a package

```bash
nix-env -e <package>
```
