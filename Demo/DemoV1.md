# Demo first release

1. clone the repository nixpkgs_swh

2. switch to branch fetchswh_test

3. Be assured that the package is already cooked (you can check it on software heritage website) or with the following GET command: 
   `curl -X GET "https://archive.softwareheritage.org/api/1/vault/directory/977fc4b98c0e85816348cebd3b12026407c368b6/"`

4. If not cooked, cook it via software heritage website or with the following POST command:
   `curl -X POST "https://archive.softwareheritage.org/api/1/vault/directory/977fc4b98c0e85816348cebd3b12026407c368b6/"`

5. Once it is cooked, execute the following commands wherever you want

6. ```bash
   export NIXPKGS=/path/to/nixpkgs_swh/
   nix-build $NIXPKGS -A hello-swh --show-trace
   ```


